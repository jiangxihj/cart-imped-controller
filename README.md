Impedance controller develop by Jin Hu during my visit in UTS. Its main application is robot assisted rehabilitation. 

It follows the principal idea of impedance control of hogan. It also has inertia decoupling and force estimation, which is described in the paper "Ficuciello F, Romano A, Villani L, et al. Cartesian impedance control of redundant manipulators for human-robot co-manipulation[C]//Intelligent Robots and Systems (IROS 2014), 2014 IEEE/RSJ International Conference on. IEEE, 2014: 2120-2125." 

The controller consist of six packages: 

* **ee_cart_imped_control**: contains all the code for impedance controller --- _basic impedance controller with inertia decoupling and force estimation \ trajectory track through nearest point search \ tangential flow field generation to assist patient_. Currently demo trajectory only apply in right arm of PR2. Run `roslaunch ee_cart_imped_control r_arm_cart_imped_controller.launch`

* **ee_cart_imped_action**: action interface used to communicate with controller, set equilibrium point and stiffness manually, borrowed from [MIT impedance controller](http://wiki.ros.org/ee_cart_imped)

* **ee_cart_imped_display**: display the current position of gripper and its equilibrium point the gripper try to track. It work as a rviz plugin. Run `rosrun rviz rviz` in termainl.
Show the demo trajectory. Run `rosrun ee_cart_imped_display show_trajectory` in another terminal. Before run show_trajectory node, make sure the trajectory file name you want to display is same in ee_cart_imped_control package

* **ee_cart_imped_extend**: It contain the rhythmic dynamic movement primitive code, which can easily read weights of recorded movement and generate the reproduction movement. It also contain some node for testing the usage of other package such as pcl kdtree and kdl dynamic solver. 

* **ee_cart_imped_launch**: Launch file to open controller and action interface for both arm 

* **ee_cart_imped_msgs**: message file and action file used across whole project