#ifndef SHOW_TRAJECTORY_H
#define SHOW_TRAJECTORY_H

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_listener.h>

#include <cmath>

#include <ee_cart_imped_msgs/EECartImpedFeedback.h>

//Used to read pre-stored trajectory file
//C++ file output/input
#include <iostream>
#include <fstream>
//Ros package
#include <ros/package.h>

class showTrajcetory
{
public:
  showTrajcetory(ros::NodeHandle &n);
  ~showTrajcetory();
private:
  ros::NodeHandle node_;
  ros::Subscriber sub_controller_state_;
  ros::Publisher marker_pub;
  std::vector<double> x_vector;
  std::vector<double> y_vector;
  std::vector<double> z_vector;
  void showTrajectoryCallBack (const ee_cart_imped_msgs::EECartImpedFeedbackConstPtr &msg);
};

#endif
