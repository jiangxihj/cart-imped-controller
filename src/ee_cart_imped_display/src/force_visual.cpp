/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <rviz/ogre_helpers/arrow.h>
#include <rviz/ogre_helpers/shape.h>
#include <rviz/ogre_helpers/object.h>

#include "ee_cart_imped_display/force_visual.h"

namespace ee_cart_imped_display
{

// BEGIN_TUTORIAL
ForceVisual::ForceVisual( Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node )
{
	scene_manager_ = scene_manager;

	// Ogre::SceneNode s form a tree, with each node storing the
	// transform (position and orientation) of itself relative to its
	// parent.  Ogre does the math of combining those transforms when it
	// is time to render.
	//
	// Here we create a node to store the pose of the Imu's header frame
	// relative to the RViz fixed frame.
	frame_node_ = parent_node->createChildSceneNode();

	// We create the arrow object within the frame node so that we can
	// set its position and direction relative to its header frame.
	force_arrow_.reset(new rviz::Arrow( scene_manager_, frame_node_ ));

	// We create the sphere object(represent the current position and attractive point)
	// within the frame node so that we can
	// set its position and direction relative to its header frame.
	current_point_.reset(new rviz::Shape( rviz::Shape::Sphere , scene_manager_, frame_node_ ));
	nearest_point_.reset(new rviz::Shape( rviz::Shape::Sphere , scene_manager_, frame_node_ ));
}

ForceVisual::~ForceVisual()
{
	// Destroy the frame node since we don't need it anymore.
	scene_manager_->destroySceneNode( frame_node_ );
}

void ForceVisual::setMessage( const ee_cart_imped_msgs::EECartImpedFeedback::ConstPtr& msg, const Ogre::Vector3& position)
{
	geometry_msgs::Vector3 a;
	a.x = msg->actual_pose.wrench_or_stiffness.force.x;
	a.y = msg->actual_pose.wrench_or_stiffness.force.y;
	a.z = msg->actual_pose.wrench_or_stiffness.force.z;

	//Convert the geometry_msgs::Vector3 to an Ogre::Vector3.
	Ogre::Vector3 acc( a.x, a.y, a.z );

	// Scale the arrow's thickness in each dimension along with its length.
	Ogre::Vector3 scale_arrow( 0.05, 0.05, 0.05  );
	force_arrow_->setScale( scale_arrow );

	// Set the orientation of the arrow to match the direction of the
	// acceleration vector.
	force_arrow_->setDirection( acc );

	//Draw the current position of the end-effector
	Ogre::Vector3 scale_point( 0.025, 0.025, 0.025 );
	current_point_->setScale(scale_point);
	nearest_point_->setScale(scale_point);

	Ogre::Vector3 attract_position(-1 * (msg->attract_point.position.x),
	                               -1 * (msg->attract_point.position.y),
	                               -1 * (msg->attract_point.position.z)); //hack way to display z
	nearest_point_->setPosition(attract_position);

}

// Position and orientation are passed through to the SceneNode.
void ForceVisual::setFramePosition( const Ogre::Vector3& position )
{
	frame_node_->setPosition( position );
}

void ForceVisual::setFrameOrientation( const Ogre::Quaternion& orientation )
{
	frame_node_->setOrientation( orientation );
}

// Color is passed through to the Arrow object.
void ForceVisual::setColor( float r, float g, float b, float a )
{
	force_arrow_->setColor( r, g, b, 0.5 * a );
	current_point_->setColor( r, g, b, a );
}
// END_TUTORIAL

} // end namespace rviz_plugin_tutorials

