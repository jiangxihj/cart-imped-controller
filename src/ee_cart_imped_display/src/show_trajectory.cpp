#include "ee_cart_imped_display/show_trajectory.h"

showTrajcetory::showTrajcetory(ros::NodeHandle &n) :
  node_(n)
{
  std::string string_line;
  std::ifstream infile;
  //Should change the name of display trajectory according to the file you use in ee_cart_imped_control package
  std::string package_path_ = ros::package::getPath( "ee_cart_imped_extend" ) + "/dmp_encode_trajectory/dmp_repo_figure8.txt";
  infile.open(package_path_.c_str());
  while (std::getline(infile, string_line)) // To get you all the lines.
  {
    double x, y, z, xdot, ydot;
    int index;
    sscanf(string_line.c_str(), "%d | %lf | %lf | %lf | %lf | %lf", &index, &x, &y, &xdot, &ydot);
    z = -0.15;

    x_vector.push_back(x);
    y_vector.push_back(y);
    z_vector.push_back(z);
  }
  infile.close();
  ROS_INFO_STREAM("Start display the demo trajectory");

  marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);
  sub_controller_state_ = n.subscribe("/r_arm_cart_imped_controller/state", 1, &showTrajcetory::showTrajectoryCallBack, this);
}

void showTrajcetory::showTrajectoryCallBack (const ee_cart_imped_msgs::EECartImpedFeedbackConstPtr &msg)
{
  // %Tag(MARKER_INIT)%
  visualization_msgs::Marker line_strip;
  line_strip.header.frame_id = "/torso_lift_link";
  line_strip.header.stamp = ros::Time::now();
  line_strip.ns = "points_and_lines";
  line_strip.action = visualization_msgs::Marker::ADD;
  line_strip.pose.orientation.w = 1.0;
// %EndTag(MARKER_INIT)%

// %Tag(ID)%
  line_strip.id = 1;
// %EndTag(ID)%

// %Tag(TYPE)%
  line_strip.type = visualization_msgs::Marker::LINE_STRIP;
// %EndTag(TYPE)%

// %Tag(SCALE)%
  // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
  line_strip.scale.x = 0.005;
// %EndTag(SCALE)%

  // Line strip is blue
  line_strip.color.b = 1.0;
  line_strip.color.a = 1.0;

// %EndTag(COLOR)%

// %Tag(HELIX)%
  // Create the vertices for the points and lines
  for (uint32_t i = 0; i < x_vector.size(); ++i)
  {
    float x = x_vector[i] + msg->initial_pose.position.x;
    float y = y_vector[i] + msg->initial_pose.position.y;
    float z = z_vector[i] + msg->initial_pose.position.z;

    geometry_msgs::Point p;
    p.x = x;
    p.y = y;
    p.z = z;

    line_strip.points.push_back(p);
  }
// %EndTag(HELIX)%
  marker_pub.publish(line_strip);
}

showTrajcetory::~showTrajcetory()
{
}

int main( int argc, char** argv )
{
  ros::init(argc, argv, "points_and_lines");
  ros::NodeHandle n;
  showTrajcetory st(n);

  ros::spin();
  return 0;
}


