#include "ee_cart_imped_control/ee_cart_imped_control.h"
using namespace ee_cart_imped_control_ns;

void EECartImpedControlClass::InertiaMatrixKDLToEigen(KDL::JntSpaceInertiaMatrix &m, JointInertiaMatrix &mass)
{
	for (int i = 0; i < m.data.rows(); i++)
	{
		for (int j = 0; j < m.data.cols(); j++)
		{
			mass(i, j) = m.data(i, j);
		}
	}
}

void EECartImpedControlClass::ArrayKDLToEigen(KDL::JntArray const &a, JointVector &v)
{
	for (int i = 0; i < a.data.rows(); i++)
	{
		for (int j = 0; j < a.data.cols(); j++)
		{
			v(i, j) = a.data(i, j);
		}
	}
}

void EECartImpedControlClass::JacobianMatrixKDLToEigen(KDL::Jacobian &jac, JacobianMatrix &j_e)
{
	for (int i = 0; i < jac.data.rows(); i++)
	{
		for (int j = 0; j < jac.data.cols(); j++)
		{
			j_e(i, j) = jac.data(i, j);
		}
	}
}

void EECartImpedControlClass::ArrayKDLToEigen(KDL::JntArrayVel const &a, JointVector &v)
{
	for (int i = 0; i < a.qdot.data.rows(); i++)
	{
		for (int j = 0; j < a.qdot.data.cols(); j++)
		{
			v(i, j) = a.qdot.data(i, j);
		}
	}
}

void EECartImpedControlClass::TwistKDLToEigen(KDL::Twist &t, Cart6Vector &v)
{
	for (int i = 0; i < 6; i++)
	{
		v[i] = t[i];
	}
}

void EECartImpedControlClass::PseudoInverse(CartesianInertiaMatrix const & a,
        double epsilon,
        CartesianInertiaMatrix & inv)
{
	Eigen::JacobiSVD<CartesianInertiaMatrix, Eigen::HouseholderQRPreconditioner> svd(a , Eigen::ComputeFullU | Eigen::ComputeFullV);
	double tolerance = epsilon * std::max(a.cols(), a.rows()) * svd.singularValues().array().abs()(0);
	inv = svd.matrixV() *  (svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0).matrix().asDiagonal() * svd.matrixU().adjoint();
}

double EECartImpedControlClass::linearlyInterpolate(double time,
        double startTime,
        double endTime,
        double startValue,
        double endValue) {
	return startValue +
	       (time - startTime) *
	       (endValue - startValue) / (endTime - startTime);
}

ee_cart_imped_msgs::StiffPoint
EECartImpedControlClass::sampleInterpolation()
{

	boost::shared_ptr<const EECartImpedData> desired_poses_ptr;
	desired_poses_box_.get(desired_poses_ptr);
	if (!desired_poses_ptr) {
		ROS_FATAL("ee_cart_imped_control: current trajectory was NULL!");
	}
	const EECartImpedTraj &desiredPoses = desired_poses_ptr->traj;
	const ee_cart_imped_msgs::StiffPoint &initial_point =
	    desired_poses_ptr->initial_point;
	const ros::Time &current_goal_start_time = desired_poses_ptr->starting_time;
	if (desiredPoses.size() == 0) {
		ROS_ERROR("ee_cart_imped_control: Empty trajectory");
		return last_point_;
	}
	if (last_goal_starting_time_ != current_goal_start_time.toSec()) {
		//we have never seen this goal before
		last_point_ = initial_point;
		last_point_.time_from_start = ros::Duration(0);
	}
	last_goal_starting_time_ = current_goal_start_time.toSec();

	// time from the start of the series of points
	double time = robot_state_->getTime().toSec();
	double timeFromStart = time - current_goal_start_time.toSec();
	ee_cart_imped_msgs::StiffPoint next_point;


	//Find the correct trajectory segment to use
	//We don't want a current_goal_index_ because it has
	//the potential to get caught in a bad race condition
	int current_goal_index;
	for (current_goal_index = 0;
	        current_goal_index < (signed int)desiredPoses.size();
	        current_goal_index++) {
		if (desiredPoses[current_goal_index].time_from_start.toSec() >= timeFromStart) {
			break;
		}
	}

	if (current_goal_index >= (signed int)desiredPoses.size()) {
		//we're done with the goal, hold the last position
		return desiredPoses[current_goal_index - 1];
	}

	//did we move on to the next point?
	if (current_goal_index > 0 && last_point_.time_from_start.toSec() !=
	        desiredPoses[current_goal_index - 1].time_from_start.toSec()) {
		//this should be where we CURRENTLY ARE
		last_point_.pose.position.x = x_.p(0);
		last_point_.pose.position.y = x_.p(1);
		last_point_.pose.position.z = x_.p(2);
		x_.M.GetQuaternion(last_point_.pose.orientation.x,
		                   last_point_.pose.orientation.y,
		                   last_point_.pose.orientation.z,
		                   last_point_.pose.orientation.w);
		last_point_.wrench_or_stiffness =
		    desiredPoses[current_goal_index - 1].wrench_or_stiffness;
		last_point_.isForceX = desiredPoses[current_goal_index - 1].isForceX;
		last_point_.isForceY = desiredPoses[current_goal_index - 1].isForceY;
		last_point_.isForceZ = desiredPoses[current_goal_index - 1].isForceZ;
		last_point_.isTorqueX = desiredPoses[current_goal_index - 1].isTorqueX;
		last_point_.isTorqueY = desiredPoses[current_goal_index - 1].isTorqueY;
		last_point_.isTorqueZ = desiredPoses[current_goal_index - 1].isTorqueZ;
		last_point_.time_from_start =
		    desiredPoses[current_goal_index - 1].time_from_start;
	}

	ee_cart_imped_msgs::StiffPoint start_point;
	const ee_cart_imped_msgs::StiffPoint &end_point =
	    desiredPoses[current_goal_index];
	//actually now last_point_ and initial point should be the
	//same if current_goal_index is zero
	if (current_goal_index == 0) {
		start_point = initial_point;
	} else {
		start_point = last_point_;
	}

	double segStartTime = 0.0;
	if (current_goal_index > 0) {
		segStartTime =
		    desiredPoses[current_goal_index - 1].time_from_start.toSec();
	}
	double segEndTime =
	    desiredPoses[current_goal_index].time_from_start.toSec();
	if (segEndTime <= segStartTime) {
		//just stay where we currently are
		next_point.pose.position.x = x_.p(0);
		next_point.pose.position.y = x_.p(1);
		next_point.pose.position.z = x_.p(2);
		x_.M.GetQuaternion(next_point.pose.orientation.x,
		                   next_point.pose.orientation.y,
		                   next_point.pose.orientation.z,
		                   next_point.pose.orientation.w);

	} else {
		next_point.pose.position.x = linearlyInterpolate
		                             (timeFromStart, segStartTime, segEndTime,
		                              start_point.pose.position.x, end_point.pose.position.x);

		next_point.pose.position.y = linearlyInterpolate
		                             (timeFromStart, segStartTime, segEndTime,
		                              start_point.pose.position.y, end_point.pose.position.y);

		next_point.pose.position.z = linearlyInterpolate
		                             (timeFromStart, segStartTime, segEndTime,
		                              start_point.pose.position.z, end_point.pose.position.z);

		next_point.pose.orientation.x = linearlyInterpolate
		                                (timeFromStart, segStartTime, segEndTime,
		                                 start_point.pose.orientation.x, end_point.pose.orientation.x);

		next_point.pose.orientation.y = linearlyInterpolate
		                                (timeFromStart, segStartTime, segEndTime,
		                                 start_point.pose.orientation.y, end_point.pose.orientation.y);

		next_point.pose.orientation.z = linearlyInterpolate
		                                (timeFromStart, segStartTime, segEndTime,
		                                 start_point.pose.orientation.z, end_point.pose.orientation.z);

		next_point.pose.orientation.w = linearlyInterpolate
		                                (timeFromStart, segStartTime, segEndTime,
		                                 start_point.pose.orientation.w, end_point.pose.orientation.w);
	}
	//we don't currently interpolate between wrench
	//and stiffness as generally the user wants one
	//wrench through a full trajectory point and then
	//another at the next trajectory point
	//perhaps, however, we should put in some interpolation
	//to avoid fast transitions between very different wrenches
	//as these can lead to bad behavior
	next_point.wrench_or_stiffness =
	    desiredPoses[current_goal_index].wrench_or_stiffness;
	next_point.isForceX = desiredPoses[current_goal_index].isForceX;
	next_point.isForceY = desiredPoses[current_goal_index].isForceY;
	next_point.isForceZ = desiredPoses[current_goal_index].isForceZ;
	next_point.isTorqueX = desiredPoses[current_goal_index].isTorqueX;
	next_point.isTorqueY = desiredPoses[current_goal_index].isTorqueY;
	next_point.isTorqueZ = desiredPoses[current_goal_index].isTorqueZ;
	next_point.time_from_start = ros::Duration(timeFromStart);
	return next_point;
}

void EECartImpedControlClass::commandCB(const ee_cart_imped_msgs::EECartImpedGoalConstPtr &msg)
{
	if ((msg->trajectory).empty()) {
		//stop the controller
		starting();
		return;
	}
	//this is a new goal
	boost::shared_ptr<EECartImpedData> new_traj_ptr
	(new EECartImpedData());
	if (!new_traj_ptr) {
		ROS_ERROR("Null new trajectory.");
		starting();
		return;
	}

	EECartImpedData &new_traj = *new_traj_ptr;
	KDL::Frame init_pos;
	KDL::JntArray q0(kdl_chain_.getNrOfJoints());
	KDL::ChainFkSolverPos_recursive fksolver(kdl_chain_);
	//Operation is in fact const (although not listed as such)
	read_only_chain_.getPositions(q0);
	fksolver.JntToCart(q0, init_pos);

	new_traj.initial_point.pose.position.x = init_pos.p(0);
	new_traj.initial_point.pose.position.y = init_pos.p(1);
	new_traj.initial_point.pose.position.z = init_pos.p(2);
	init_pos.M.GetQuaternion(new_traj.initial_point.pose.orientation.x,
	                         new_traj.initial_point.pose.orientation.y,
	                         new_traj.initial_point.pose.orientation.z,
	                         new_traj.initial_point.pose.orientation.w);

	for (size_t i = 0; i < msg->trajectory.size(); i++) {
		new_traj.traj.push_back(msg->trajectory[i]);
	}
	if (!new_traj_ptr) {
		ROS_ERROR("Null new trajectory after filling.");
		starting();
		return;
	}
	new_traj.starting_time = ros::Time::now();
	desired_poses_box_.set(new_traj_ptr);
}

//encapsulate computation of task torque+pose torque
EECartImpedControlClass::JointVector EECartImpedControlClass::computeTaskTorque( KDL::JntArray const &q, KDL::JntArrayVel const &qdot, double timestep)
{
	JointVector qdot_e;
	ArrayKDLToEigen(qdot, qdot_e);

	// Compute the forward kinematics and Jacobian (at this location)
	KDL::Jacobian  Jtemp_(kdl_chain_.getNrOfJoints());
	jnt_to_jac_solver_->JntToJac(q, Jtemp_);
	JacobianMatrix J_e, J_last_e, J_delta, J_dot;//J_bar_transpose
	JacobianMatrixKDLToEigen(Jtemp_, J_e);
	JacobianMatrixKDLToEigen(J_last, J_last_e);

	//Compute the offset of jacobian matrix
	J_delta = J_e - J_last_e;
	J_dot = J_delta / timestep;
	J_last = Jtemp_;

	//Compute catesian velocity
	Cart6Vector xdot_e;
	xdot_e = J_e * qdot_e;

	//Compute dynamic parameter: M(inertia matrix) C(coriolis matrix) G(gravity matrix) in joint space
	KDL::JntSpaceInertiaMatrix M_(kdl_chain_.getNrOfJoints());
	MCG_solver_->JntToMass(q, M_);
	KDL::JntArray C_(kdl_chain_.getNrOfJoints());
	MCG_solver_->JntToCoriolis(q, qdot.qdot, C_); //coriolis matrix seems worthless

	//KDL matrix to eigen matrix to easily utilize linear algebra
	JointInertiaMatrix M_e;
	InertiaMatrixKDLToEigen(M_, M_e);
	JointVector C_e;
	ArrayKDLToEigen(C_, C_e);

	//Compute cartesian space inertia matrix
	JointInertiaMatrix M_inv;
	M_inv =  M_e.inverse();
	CartesianInertiaMatrix M_cart_inv, M_cart;
	M_cart_inv = J_e * M_inv * J_e.transpose();
	PseudoInverse(M_cart_inv, std::numeric_limits<double>::epsilon(), M_cart);

	//Compute desired mass and damping matrix
	CartesianInertiaMatrix Mass_d, Damp_d;
	Mass_d = M_des * Eigen::MatrixXd::Identity(6, 6); //actually is inverse of desired mass matrix
	Damp_d = D_des * Eigen::MatrixXd::Identity(6, 6);

	//Compute dynamiclly consistent generlized inverse matrix and projection matrix
	JacobianMatrix J_bar_transpose;
	J_bar_transpose = M_cart * J_e * M_inv;
	JointInertiaMatrix null_proj;
	null_proj = Eigen::MatrixXd::Identity(Joints, Joints) - J_e.transpose() * J_bar_transpose;

	//compute task torque
	JointVector tau_task;
	tau_task = -J_e.transpose() * M_cart * (-J_e * M_inv * C_e + J_dot * qdot_e + Mass_d.inverse() * (Damp_d * xdot_e )); // - J_bar_transpose * r_e | - r_e
	//tau_task = -J_e.transpose() * ( Mass_d.inverse() * (Damp_d * xdot_e ));
	//Compute generlized momentum
	JointVector p_e;//variable represent using eigen definition
	p_e = M_e * qdot_e;

	//Compute residual error
	for (int i = 0; i < kdl_chain_.getNrOfJoints(); i++)
	{
		sum_e(i) += (tau_task(i)  + C_e(i) + r_e(i));
		if (abs(sum_e(i)) < 0.0001)
			sum_e(i) = 0;
		r_e(i) = 100 * (-p_e(i) + sum_e(i));
		if (abs(r_e(i)) < 0.0001)
			r_e(i) = 0;
	}

	//Compute posture torque
	// JointVector tau_pose;
	// tau_pose =  - 10 * qdot_e;
	// tau_pose = null_proj * tau_pose;

	JointVector tau_sum;
	tau_sum = tau_task;// + tau_pose;
	return tau_sum;
}