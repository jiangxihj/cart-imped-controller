#include "ee_cart_imped_control/ee_cart_imped_control.h"
#include <pluginlib/class_list_macros.h>

using namespace ee_cart_imped_control_ns;

bool EECartImpedControlClass::init(pr2_mechanism_model::RobotState *robot,
                                   ros::NodeHandle &n) {
  node_ = n;
  if (!n.getParam("root_name", root_name))
  {
    ROS_ERROR("No root name given in namespace: %s)",
              n.getNamespace().c_str());
    return false;
  }
  if (!n.getParam("tip_name", tip_name))
  {
    ROS_ERROR("No tip name given in namespace: %s)",
              n.getNamespace().c_str());
    return false;
  }

  // Construct a chain from the root to the tip and prepare the kinematics
  // Note the joints must be calibrated
  if (!chain_.init(robot, root_name, tip_name))
  {
    ROS_ERROR("EECartImpedControlClass could not use the chain from '%s' to '%s'",
              root_name.c_str(), tip_name.c_str());
    return false;
  }


  if (!read_only_chain_.init(robot, root_name, tip_name))
  {
    ROS_ERROR
    ("EECartImpedControlClass could not use the chain from '%s' to '%s'",
     root_name.c_str(), tip_name.c_str());
    return false;
  }

  // Store the robot handle for later use (to get time)
  robot_state_ = robot;

  // Construct the kdl solvers in non-realtime
  chain_.toKDL(kdl_chain_);
  jnt_to_pose_solver_.reset(new KDL::ChainFkSolverPos_recursive(kdl_chain_));
  jnt_to_jac_solver_.reset(new KDL::ChainJntToJacSolver(kdl_chain_));

  //contruct dynamic parameter solver
  KDL::Vector g(0.0, 0.0, -9.81);
  MCG_solver_.reset(new KDL::ChainDynParam(kdl_chain_, g));

  // Resize (pre-allocate) the variables in non-realtime
  q_.resize(kdl_chain_.getNrOfJoints());
  qdot_.resize(kdl_chain_.getNrOfJoints());
  tau_.resize(kdl_chain_.getNrOfJoints());
  tau_act_.resize(kdl_chain_.getNrOfJoints());
  J_.resize(kdl_chain_.getNrOfJoints());
  J_last.resize(kdl_chain_.getNrOfJoints());

  subscriber_ = node_.subscribe("command", 1, &EECartImpedControlClass::commandCB, this);
  controller_state_publisher_.reset
  (new realtime_tools::RealtimePublisher
   <ee_cart_imped_msgs::EECartImpedFeedback>
   (node_, "state", 1));
  controller_state_publisher_->msg_.requested_joint_efforts.resize
  (kdl_chain_.getNrOfJoints());
  controller_state_publisher_->msg_.actual_joint_efforts.resize
  (kdl_chain_.getNrOfJoints());
  updates_ = 0;

  //Flow constant which control the amplitude of flow field
  Kd_.vel(0) = 4.0;        // Translation x
  Kd_.vel(1) = 4.0;        // Translation y
  Kd_.vel(2) = 4.0;        // Translation z
  Kd_.rot(0) = 4.0;        // Rotation x
  Kd_.rot(1) = 4.0;        // Rotation y
  Kd_.rot(2) = 4.0;        // Rotation z

  //Spring constant which control the amplitude of attract force
  Kp_.vel(0) = MAX_STIFFNESS;       // Translation x
  Kp_.vel(1) = MAX_STIFFNESS;        // Translation y
  Kp_.vel(2) = MAX_STIFFNESS;        // Translation z
  Kp_.rot(0) = ACCEPTABLE_ROT_STIFFNESS;       // Rotation x
  Kp_.rot(1) = ACCEPTABLE_ROT_STIFFNESS;       // Rotation y
  Kp_.rot(2) = ACCEPTABLE_ROT_STIFFNESS;      // Rotation z

  //Desired mass and damping term
  M_des = 5;
  D_des = 100;

  //Create a dummy trajectory
  boost::shared_ptr<EECartImpedData> dummy_ptr(new EECartImpedData());
  EECartImpedTraj &dummy = dummy_ptr->traj;
  dummy.resize(1);
  dummy[0].time_from_start = ros::Duration(0);
  desired_poses_box_.set(dummy_ptr);
  last_goal_starting_time_ = -1;

  //Initialize task torque thread and sign
  working_  = false;

  //Initialize the demo trajectory and velocity vectory(KDL type)
  // const int tau = 2;//movement period 2s
  // const double dt = 0.001;//time step
  // int n_pts = tau / dt + 1;
  // double omega = (2 * KDL::PI) / tau;
  // demo_traj.resize(n_pts);
  // demo_traj_vel.resize(n_pts);

  //Initialize the trajectory with ellipse
  // for (int i = 0; i < n_pts; i++)
  // {
  //   double x, y, z;
  //   double x_dot, y_dot, z_dot;
  //   x = -0.2 + 0.1 * sin(dt * i * omega);
  //   y = -0.2 + 0.1 * cos(dt * i * omega);
  //   z = -0.15;
  //   x_dot = 0.1 * omega * cos(dt * i * omega);
  //   y_dot = -0.1 * omega * sin(dt * i * omega);
  //   z_dot = 0;

  //   demo_traj[i].p(0) = x;
  //   demo_traj[i].p(1) = y;
  //   demo_traj[i].p(2) = z;
  //   demo_traj[i].M = KDL::Rotation::Quaternion(0, 0, 0, 1);
  //   demo_traj_vel[i].vel(0) = x_dot;
  //   demo_traj_vel[i].vel(1) = y_dot;
  //   demo_traj_vel[i].vel(2) = z_dot;
  //   demo_traj_vel[i].rot(0) = 0;
  //   demo_traj_vel[i].rot(1) = 0;
  //   demo_traj_vel[i].rot(2) = 0;
  // }

  //Initialize the trajectory with pre-stored dmp data, read from txt--------------------//
  ROS_INFO_STREAM("Start read txt file");
  std::string string_line;
  std::ifstream infile;
  /// Change to different reference trajectory is done by change the name of txt 
  // Currently there are two tarjectory: "circle trajectory(dmp_repo_circle.txt)" and "figure8 trajectory(dmp_repo_figure8.txt)"
  std::string package_path_ = ros::package::getPath( "ee_cart_imped_extend" ) + "/dmp_encode_trajectory/dmp_repo_figure8.txt";
  infile.open(package_path_.c_str());
  while(std::getline(infile, string_line)) // To get you all the lines.
  {
  	double x, y, z, xdot, ydot, zdot;
  	int index;
    sscanf(string_line.c_str(), "%d | %lf | %lf | %lf | %lf | %lf", &index, &x, &y, &xdot, &ydot);
    //Reference Trajectory is only 2D(x-y), height in z axis is set to constant 
    z = -0.15;
    zdot = 0;

    KDL::Frame point;
    point.p(0) = x;
    point.p(1) = y;
    point.p(2) = z;
    point.M = KDL::Rotation::Quaternion(0, 0, 0, 1);
    demo_traj.push_back(point);

    //Read point velocity and store it in vector
    KDL::Twist point_velocity;
    point_velocity.vel(0) = xdot;
    point_velocity.vel(1) = ydot;
    point_velocity.vel(2) = zdot;
    point_velocity.rot(0) = 0;
    point_velocity.rot(1) = 0;
    point_velocity.rot(2) = 0;
    demo_traj_vel.push_back(point_velocity);
  }
  infile.close();
  //--------------------------------------------------------------------------------------

  return true;
}

void EECartImpedControlClass::starting() {
  // Get the current joint values to compute the initial tip location.
  KDL::JntArray q0(kdl_chain_.getNrOfJoints());
  KDL::ChainFkSolverPos_recursive fksolver(kdl_chain_);
  //this operation is not listed as const but is in fact
  //in the current implementation
  read_only_chain_.getPositions(q0);
  fksolver.JntToCart(q0, init_pos);

  //Also reset the start state of jacobian
  jnt_to_jac_solver_->JntToJac(q0, J_last);

  // Also reset the time-of-last-servo-cycle
  last_time_ = robot_state_->getTime();
  ///Hold current position trajectory
  boost::shared_ptr<EECartImpedData> hold_traj_ptr(new EECartImpedData());
  if (!hold_traj_ptr) {
    ROS_ERROR("While starting, trajectory pointer was null");
    return;
  }
  EECartImpedData &hold_traj = *hold_traj_ptr;
  hold_traj.traj.resize(1);

  hold_traj.traj[0].pose.position.x = init_pos.p(0);
  hold_traj.traj[0].pose.position.y = init_pos.p(1);
  hold_traj.traj[0].pose.position.z = init_pos.p(2);
  // init_pos.M.GetQuaternion((hold_traj.traj[0].pose.orientation.x),
  //                          (hold_traj.traj[0].pose.orientation.y),
  //                          (hold_traj.traj[0].pose.orientation.z),
  //                          (hold_traj.traj[0].pose.orientation.w));
  hold_traj.traj[0].pose.orientation.x = 0;
  hold_traj.traj[0].pose.orientation.y = -0.707;
  hold_traj.traj[0].pose.orientation.z = 0;
  hold_traj.traj[0].pose.orientation.w = 0.707;
  hold_traj.traj[0].wrench_or_stiffness.force.x = MAX_STIFFNESS;
  hold_traj.traj[0].wrench_or_stiffness.force.y = MAX_STIFFNESS;
  hold_traj.traj[0].wrench_or_stiffness.force.z = MAX_STIFFNESS;
  hold_traj.traj[0].wrench_or_stiffness.torque.x = ACCEPTABLE_ROT_STIFFNESS;
  hold_traj.traj[0].wrench_or_stiffness.torque.y = ACCEPTABLE_ROT_STIFFNESS;
  hold_traj.traj[0].wrench_or_stiffness.torque.z = ACCEPTABLE_ROT_STIFFNESS;
  hold_traj.traj[0].isForceX = false;
  hold_traj.traj[0].isForceY = false;
  hold_traj.traj[0].isForceZ = false;
  hold_traj.traj[0].isTorqueX = false;
  hold_traj.traj[0].isTorqueY = false;
  hold_traj.traj[0].isTorqueZ = false;
  hold_traj.traj[0].time_from_start = ros::Duration(0);
  hold_traj.initial_point = hold_traj.traj[0];
  hold_traj.starting_time = ros::Time::now();
  if (!hold_traj_ptr) {
    ROS_ERROR("During starting hold trajectory was null after filling");
    return;
  }
  //Pass the trajectory through to the update loop
  desired_poses_box_.set(hold_traj_ptr);

  //Start the thread to compute task torque
  if (working_)
  {
    ROS_ERROR("Task computation thread already running");
    return;
  }
  working_ = true;

  // Start additional thread to compute inertia decoupling torque here(task torque)
  genThread_ = new boost::thread( &EECartImpedControlClass::doCalculation, this, 800.0);

  r_e.setZero();
  sum_e.setZero();
  tau_sum_e.setZero();

  //Initialize kd tree to compute the neareat point
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  // Generate pointcloud data
  cloud->points.resize(demo_traj.size());

  for (size_t i = 0; i < cloud->points.size (); ++i)
  {
    cloud->points[i].x = init_pos.p(0) + demo_traj[i].p(0);
    demo_traj[i].p(0) += init_pos.p(0);
    cloud->points[i].y = init_pos.p(1) + demo_traj[i].p(1);
    demo_traj[i].p(1) += init_pos.p(1);
    cloud->points[i].z = init_pos.p(2) + demo_traj[i].p(2);
    demo_traj[i].p(2) += init_pos.p(2);
  }
  ROS_INFO_STREAM("KD TREE Initialize");
  kdtree.setInputCloud(cloud);

  SetToZero(flow_velocity);
  SetToZero(flow_velocity_history);

  //Find the nearest point for initial pose and store its velocity as direction history
  searchPoint.x = init_pos.p(0);
  searchPoint.y = init_pos.p(1);
  searchPoint.z = init_pos.p(2);
  std::vector<int> pointIdxNKNSearch(1);
  std::vector<float> pointNKNSquaredDistance(1);

  if ( kdtree.nearestKSearch (searchPoint, 1, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
  {
    flow_velocity_history = demo_traj_vel[ pointIdxNKNSearch[0] ];
  }
  double norm = flow_velocity_history.vel.Norm();
  flow_velocity_history = flow_velocity_history / norm;
  point_history = pointIdxNKNSearch[0];
  ROS_INFO_STREAM("Index of initial equilibrium point: " << point_history);
  //Define search range to be whole_phase_range/20
  s_range = demo_traj.size() / 20;
}

void EECartImpedControlClass::update()
{
  dt = (robot_state_->getTime() - last_time_).toSec();
  last_time_ = robot_state_->getTime();

  // Get the current joint positions and velocities
  chain_.getPositions(q_);
  chain_.getVelocities(qdot_);

  // Compute the forward kinematics and Jacobian (at this location)
  jnt_to_pose_solver_->JntToCart(q_, x_);
  jnt_to_jac_solver_->JntToJac(q_, J_);

  //compute end-effector velocity using joint velocity and jacobian
  for (unsigned int i = 0 ; i < 6 ; i++)
  {
    xdot_(i) = 0;
    for (unsigned int j = 0 ; j < kdl_chain_.getNrOfJoints() ; j++)
      xdot_(i) += J_(i, j) * qdot_.qdot(j);
  }

  //Not suitable for our rehabilitation application
  //get target and viapoint data from action interface
  //Compute desired target through linear interpolation
  //the data can be set shown on  goal definition in file ee_cart_imped_msgs/action/EECartImped.action 
  ee_cart_imped_msgs::StiffPoint desiredPose = sampleInterpolation();


  Fdes_(0) = desiredPose.wrench_or_stiffness.force.x;
  Fdes_(1) = desiredPose.wrench_or_stiffness.force.y;
  Fdes_(2) = desiredPose.wrench_or_stiffness.force.z;
  Fdes_(3) = desiredPose.wrench_or_stiffness.torque.x;
  Fdes_(4) = desiredPose.wrench_or_stiffness.torque.y;
  Fdes_(5) = desiredPose.wrench_or_stiffness.torque.z;

  // Kp_.vel(0) = desiredPose.wrench_or_stiffness.force.x;
  // Kp_.vel(1) = desiredPose.wrench_or_stiffness.force.y;
  // Kp_.vel(2) = desiredPose.wrench_or_stiffness.force.z;
  // Kp_.rot(0) = desiredPose.wrench_or_stiffness.torque.x;
  // Kp_.rot(1) = desiredPose.wrench_or_stiffness.torque.y;
  // Kp_.rot(2) = desiredPose.wrench_or_stiffness.torque.z;

  // xd_.p(0) = desiredPose.pose.position.x;
  // xd_.p(1) = desiredPose.pose.position.y;
  // xd_.p(2) = desiredPose.pose.position.z;
  // xd_.M = KDL::Rotation::Quaternion(desiredPose.pose.orientation.x,
  //                                   desiredPose.pose.orientation.y,
  //                                   desiredPose.pose.orientation.z,
  //                                   desiredPose.pose.orientation.w);

  //K nearest neighbor search------------------------------------//
  //set current position as search point
  searchPoint.x = x_.p(0);
  searchPoint.y = x_.p(1);
  searchPoint.z = x_.p(2);
  const int K = 15;
  std::vector<int> pointIdxNKNSearch(K);
  std::vector<float> pointNKNSquaredDistance(K);
  unsigned int point_index = 0;
  float distance_nearest = 0.0;

  //Get the index and distance of the most related direction point
  if ( kdtree.nearestKSearch (searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
  {
  	//point_index = pointIdxNKNSearch[0];
    point_index = point_history;
  	//distance_nearest = pointNKNSquaredDistance[0];

  	KDL::Twist vel_normlized = demo_traj_vel[ pointIdxNKNSearch[0] ] / ( demo_traj_vel[ pointIdxNKNSearch[0] ].vel.Norm() );
  	double max_velocity_dot_product = dot(vel_normlized.vel, flow_velocity_history.vel);
  	KDL::Twist temp_vel_normlized;
  	double temp_velocity_dot_product;
    //Case 1: search segment do not cover initial point, search from[history history+search_range]
    if ( (point_history + s_range) < demo_traj.size() )
    {
      for (size_t i = 0; i < pointIdxNKNSearch.size (); ++i)
      {
        temp_vel_normlized = demo_traj_vel[ pointIdxNKNSearch[i] ] / ( demo_traj_vel[ pointIdxNKNSearch[i] ].vel.Norm() );
        temp_velocity_dot_product = dot(temp_vel_normlized.vel, flow_velocity_history.vel);
        //make search range be [point_history+1, point_history+s_range]
        if ((temp_velocity_dot_product > max_velocity_dot_product) && (pointIdxNKNSearch[i] > point_history) && (pointIdxNKNSearch[i] < (point_history + s_range)) )
        {
          point_index = pointIdxNKNSearch[i];
          distance_nearest = pointNKNSquaredDistance[i];
          max_velocity_dot_product = temp_velocity_dot_product;
        }
      }
    }
  	else //Case 2: search segment cover initial point
    {
      //Case 2.1: history get to the end of one trajectory, then search from [0 search_range]
      if ( (point_history + 1) == demo_traj.size() )
      {
        for (size_t i = 0; i < pointIdxNKNSearch.size (); ++i)
        {
          temp_vel_normlized = demo_traj_vel[ pointIdxNKNSearch[i] ] / ( demo_traj_vel[ pointIdxNKNSearch[i] ].vel.Norm() );
          temp_velocity_dot_product = dot(temp_vel_normlized.vel, flow_velocity_history.vel);
          //make search range be [0, 0+s_range]
          if ((temp_velocity_dot_product > max_velocity_dot_product) && (pointIdxNKNSearch[i] >= 0) && (pointIdxNKNSearch[i] < (0 + s_range)) )
          {
            point_index = pointIdxNKNSearch[i];
            distance_nearest = pointNKNSquaredDistance[i];
            max_velocity_dot_product = temp_velocity_dot_product;
          }
        }
      }
      //Case 2.1: search segment cutted into twp part, search from [history+1 trajectory_end] or [0 history+100-trajectory_size]
      else
      {
        for (size_t i = 0; i < pointIdxNKNSearch.size (); ++i)
        {
          temp_vel_normlized = demo_traj_vel[ pointIdxNKNSearch[i] ] / ( demo_traj_vel[ pointIdxNKNSearch[i] ].vel.Norm() );
          temp_velocity_dot_product = dot(temp_vel_normlized.vel, flow_velocity_history.vel);
          //make search range be [point_history, point_history+100]
          if ( (temp_velocity_dot_product > max_velocity_dot_product) && ( ((pointIdxNKNSearch[i] > point_history) && (pointIdxNKNSearch[i] < demo_traj.size())) || ((pointIdxNKNSearch[i] >= 0) && (pointIdxNKNSearch[i] <= (point_history + s_range - demo_traj.size()))) ) )
          {
            point_index = pointIdxNKNSearch[i];
            distance_nearest = pointNKNSquaredDistance[i];
            max_velocity_dot_product = temp_velocity_dot_product;
          }
        }
      }   
    }
  }
  //Use direction mostly related(maximium velocity dot product with history velocity) point as desired point
  xd_.p(0) = demo_traj[ point_index ].p(0);
  xd_.p(1) = demo_traj[ point_index ].p(1);
  xd_.p(2) = demo_traj[ point_index ].p(2);
  xd_.M = KDL::Rotation::Quaternion(desiredPose.pose.orientation.x,
                                    desiredPose.pose.orientation.y,
                                    desiredPose.pose.orientation.z,
                                    desiredPose.pose.orientation.w);

  flow_velocity = demo_traj_vel[ point_index ];
  xdot_ -= flow_velocity;
  flow_velocity_history = flow_velocity / flow_velocity.vel.Norm();
  double factor = exp(-(10 * distance_nearest));
  xdot_ = xdot_ * factor;

  point_history = point_index;
  // if ( (point_history+1) == demo_traj.size())
  // {
  //   point_history = 0;
  // }
  //-------------------------------------------------------------

  // Calculate a Cartesian restoring force.
  //compute position error
  xerr_.vel = x_.p - xd_.p;
  //Use cross product to represent rotation error
  xerr_.rot = 0.5 * (xd_.M.UnitX() * x_.M.UnitX() +
                     xd_.M.UnitY() * x_.M.UnitY() +
                     xd_.M.UnitZ() * x_.M.UnitZ());  


  // F_ is a vector of forces/wrenches corresponding to x, y, z, tx,ty,tz,tw
  if (desiredPose.isForceX) {
    F_(0) = Fdes_(0);
  } else {
    F_(0) = -Kp_(0) * xerr_(0) - Kd_(0) * xdot_(0);
  }

  if (desiredPose.isForceY) {
    F_(1) = Fdes_(1);
  } else {
    F_(1) = -Kp_(1) * xerr_(1) - Kd_(1) * xdot_(1);
  }

  if (desiredPose.isForceZ) {
    F_(2) = Fdes_(2);
  } else {
    F_(2) = -Kp_(2) * xerr_(2) - Kd_(2) * xdot_(2);
  }

  if (desiredPose.isTorqueX) {
    F_(3) = Fdes_(3);
  } else {
    F_(3) = -Kp_(3) * xerr_(3) - Kd_(3) * xdot_(3);
  }

  if (desiredPose.isTorqueY) {
    F_(4) = Fdes_(4);
  } else {
    F_(4) = -Kp_(4) * xerr_(4) - Kd_(4) * xdot_(4);
  }

  //Set the constant to be zero,which means do not take the rotation error in z axis into account
  //Make end-effector can rotate around z axis, but can not rotate around x/y axis 
  Kp_(5) = 0;
  if (desiredPose.isTorqueZ) {
    F_(5) = Fdes_(5);
  } else {
    F_(5) = -Kp_(5) * xerr_(5) - Kd_(5) * xdot_(5);
  }

  // Convert the force into a set of joint torques
  // tau_ is a vector of joint torques q1...qn
  for (unsigned int i = 0 ; i < kdl_chain_.getNrOfJoints() ; i++)
  {
    tau_(i) = 0;
    for (unsigned int j = 0 ; j < 6 ; j++)
    {
      tau_(i) += J_(j, i) * F_(j);
    }
  }

  // {
  //   boost::mutex::scoped_lock lock( data_mutex_ );
  //   // read task torque
  //   for (unsigned int i = 0 ; i < kdl_chain_.getNrOfJoints() ; i++)
  //   {
  //     //Need to comment when use equilibrium point
  //     //tau_(i) = 0;
  //     tau_(i) += tau_sum_e(i);
  //   }
  // }


  // And finally send these torques out
  chain_.setEfforts(tau_);

  //publish the current state
  if (!(updates_ % 10)) {
    if (controller_state_publisher_ &&
        controller_state_publisher_->trylock()) {
      controller_state_publisher_->msg_.header.stamp = last_time_;
      controller_state_publisher_->msg_.header.frame_id = tip_name;
      controller_state_publisher_->msg_.desired =
        desiredPose;
      controller_state_publisher_->msg_.actual_pose.pose.position.x =
        x_.p.x();
      controller_state_publisher_->msg_.actual_pose.pose.position.y =
        x_.p.y();
      controller_state_publisher_->msg_.actual_pose.pose.position.z =
        x_.p.z();
      x_.M.GetQuaternion
      (controller_state_publisher_->msg_.actual_pose.pose.orientation.x,
       controller_state_publisher_->msg_.actual_pose.pose.orientation.y,
       controller_state_publisher_->msg_.actual_pose.pose.orientation.z,
       controller_state_publisher_->msg_.actual_pose.pose.orientation.w);
      controller_state_publisher_->msg_.actual_pose.
      wrench_or_stiffness.force.x = F_(0);
      controller_state_publisher_->msg_.actual_pose.
      wrench_or_stiffness.force.y = F_(1);
      controller_state_publisher_->msg_.actual_pose.
      wrench_or_stiffness.force.z = F_(2);
      controller_state_publisher_->msg_.actual_pose.
      wrench_or_stiffness.torque.x = F_(3);
      controller_state_publisher_->msg_.actual_pose.
      wrench_or_stiffness.torque.y = F_(4);
      controller_state_publisher_->msg_.actual_pose.
      wrench_or_stiffness.torque.z = F_(5);
      controller_state_publisher_->msg_.attract_point.position.x = xerr_(0);
      controller_state_publisher_->msg_.attract_point.position.y = xerr_(1);
      controller_state_publisher_->msg_.attract_point.position.z = xerr_(2);

      controller_state_publisher_->msg_.initial_pose.position.x = init_pos.p(0);
      controller_state_publisher_->msg_.initial_pose.position.y = init_pos.p(1);
      controller_state_publisher_->msg_.initial_pose.position.z = init_pos.p(2);
      chain_.getEfforts(tau_act_);
      double eff_err = 0;
      for (unsigned int i = 0; i < kdl_chain_.getNrOfJoints(); i++) {
        eff_err += (tau_(i) - tau_act_(i)) * (tau_(i) - tau_act_(i));
        controller_state_publisher_->msg_.requested_joint_efforts[i] =
          tau_(i);
        controller_state_publisher_->msg_.actual_joint_efforts[i] =
          tau_act_(i);
      }
      controller_state_publisher_->msg_.effort_sq_error = eff_err;
      boost::shared_ptr<const EECartImpedData> desired_poses_ptr;
      desired_poses_box_.get(desired_poses_ptr);
      controller_state_publisher_->msg_.goal = desired_poses_ptr->traj;
      controller_state_publisher_->msg_.initial_point = last_point_;
      controller_state_publisher_->msg_.running_time =
        robot_state_->getTime() - desired_poses_ptr->starting_time;
      controller_state_publisher_->unlockAndPublish();
    }
  }
  updates_++;
}

//Task torque computation function
void EECartImpedControlClass::doCalculation(double sampling_freq)
{
  // set delteTime to the time slice for your
  ROS_INFO_STREAM( "Start task torque computation thread" );
  timespec ts_0, ts_1;
  double deltaTime = (1 / sampling_freq);
  ROS_INFO_STREAM( "sampling time: " << deltaTime );
  KDL::JntArray q0(kdl_chain_.getNrOfJoints());
  KDL::JntArrayVel q0dot(kdl_chain_.getNrOfJoints());
  JointVector tau0_;
  tau0_.setZero();

  while (working_)
  {
    clock_gettime( CLOCK_MONOTONIC, &ts_0 );
    // do your calculation here
    read_only_chain_.getPositions(q0);
    read_only_chain_.getVelocities(q0dot);
    tau0_ = computeTaskTorque(q0, q0dot, deltaTime);
    // calculate time diff
    clock_gettime( CLOCK_MONOTONIC, &ts_1 );
    timespec time_diff;
    if ((ts_1.tv_nsec - ts_0.tv_nsec) < 0)
    {
      time_diff.tv_sec = ts_1.tv_sec - ts_0.tv_sec - 1;
      time_diff.tv_nsec = 1E9 + ts_1.tv_nsec - ts_0.tv_nsec;
    }
    else
    {
      time_diff.tv_sec = ts_1.tv_sec - ts_0.tv_sec;
      time_diff.tv_nsec = ts_1.tv_nsec - ts_0.tv_nsec;
    }
    double td = time_diff.tv_sec + (double)time_diff.tv_nsec / 1E9;
    if (td > deltaTime)
    {
      ROS_WARN( "calculation time is longer than required sampling time." );
    }
    else
    {
      usleep( long((deltaTime - td) * 1E6 ) );
    }

    {
      boost::mutex::scoped_lock lock( data_mutex_ );
      // make sure you update the task torque here
      for (unsigned int j = 0 ; j < kdl_chain_.getNrOfJoints() ; j++)
      {
        tau_sum_e(j) = tau0_(j);
      }
    }
  }
}

void EECartImpedControlClass::stopping() {
  //starting();

  working_ = false;
  if (genThread_)
  {
    genThread_->join(); // let task torque computation finish
    delete genThread_; // not really necessary
  }
}

/// Register controller to pluginlib
PLUGINLIB_EXPORT_CLASS(ee_cart_imped_control_ns::EECartImpedControlClass, pr2_controller_interface::Controller)

