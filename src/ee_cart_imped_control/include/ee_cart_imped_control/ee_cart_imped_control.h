#ifndef __EE_CART_IMPED_CONTROL_H__
#define __EE_CART_IMPED_CONTROL_H__

#include <ros/ros.h>

//PR2 related
#include <pr2_controller_interface/controller.h>
#include <pr2_mechanism_model/chain.h>
#include <pr2_mechanism_model/robot.h>

//KDL related
#include <kdl/chain.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/frames.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/jntspaceinertiamatrix.hpp>
#include <kdl/chaindynparam.hpp>

//ROS realtime tool
#include <realtime_tools/realtime_publisher.h>
#include <realtime_tools/realtime_box.h>

//msg and action file in ee_cart_imped_msg package
#include <ee_cart_imped_msgs/EECartImpedAction.h>
#include <ee_cart_imped_msgs/EECartImpedGoal.h>
#include <ee_cart_imped_msgs/StiffPoint.h>

//eigen related
#include <Eigen/Geometry>
#include <Eigen/LU>
#include <Eigen/SVD>

//boost related
#include <boost/scoped_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

//KD tree in pcl
#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>

//Used to read pre-stored trajectory file
//C++ file output/input
#include <iostream>
#include <fstream>
//Ros package
#include <ros/package.h>

///Maximum stiffness for translation
#define MAX_STIFFNESS 1000.0

///Maximum stiffness for rotation
#define ACCEPTABLE_ROT_STIFFNESS 100.0

namespace ee_cart_imped_control_ns {

  /**
   * \brief Generate attract force and flow field to help stroke person do rehabilitation
   *
   * This class contains a impedance controller with basic impedance controller 
   * with inertia decoupling and force estimation \ trajectory track through nearest point search \ tangential flow field generation to assist patient. 
   * Currently demo trajectory only apply in right arm of PR2.
   * 
   */
  class EECartImpedControlClass: public pr2_controller_interface::Controller {
    enum
    {
        Joints = 7

    };

    /**
     * Define the joint/cart vector types accordingly (using a fixed
     * size to avoid dynamic allocations and make the code realtime safe).
     */
    typedef Eigen::Matrix<double, Joints, 1>  JointVector;
    typedef Eigen::Matrix<double, 6, 1>       Cart6Vector;
    typedef Eigen::Matrix<double, 6, Joints>  JacobianMatrix;
    typedef Eigen::Matrix<double, Joints, 6>  JacobianInverseMatrix;
    typedef Eigen::Matrix<double, Joints, Joints>  JointInertiaMatrix;
    typedef Eigen::Matrix<double, 6, 6>  CartesianInertiaMatrix;


    /**
     * Ensure 128-bit alignment for Eigen
     * See also http://eigen.tuxfamily.org/dox/StructHavingEigenMembers.html
     */
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  private:
    
    /**
     * The current robot state 
     * Read-only after initialization
     */
    pr2_mechanism_model::RobotState* robot_state_;
    
    /// The chain of links and joints in PR2 language for reference and commanding
    pr2_mechanism_model::Chain chain_;
    /// The chain of links and joints in PR2 language for reference only
    //Read-only after initialization
    pr2_mechanism_model::Chain read_only_chain_;
    /// The chain of links and joints in KDL language
    // Read-only after initialization
    KDL::Chain kdl_chain_;
    
    /// KDL Solver performing the joint angles to Cartesian pose calculation
    // Referenced only in update loop
    boost::scoped_ptr<KDL::ChainFkSolverPos>    jnt_to_pose_solver_;
    /// KDL Solver performing the joint angles to Jacobian calculation
    // Referenced only in update loop
    boost::scoped_ptr<KDL::ChainJntToJacSolver> jnt_to_jac_solver_;
    /// KDL Solver to calculate the matrices M (inertia),C(coriolis) and G(gravitation) in joint space
    // Referenced only in update loop
    boost::scoped_ptr<KDL::ChainDynParam> MCG_solver_;
    
    /**
     * The variables (which are declared as class variables
     * because they need to be pre-allocated, meaning we must
     * be very careful how we access them to avoid race conditions)
     */
    
    /// Joint positions
    KDL::JntArray  q_; 
    /// Joint velocities
    KDL::JntArrayVel  qdot_;
    /// Commanded and actual Joint torques   
    KDL::JntArray  tau_, tau_act_;
    /// Joint troque vector in eigen form
    JointVector tau_sum_e;
    
    /// Tip pose
    KDL::Frame     x_; 
    /// Tip desired pose          
    KDL::Frame     xd_;
    
    /// Cartesian error
    KDL::Twist     xerr_;
    /// Cartesian velocity
    KDL::Twist     xdot_;
    /// Cartesian effort
    KDL::Wrench    F_;  
    /// Desired Cartesian force, set manully using action intreface
    KDL::Wrench    Fdes_;
    /// Jacobian in current time
    KDL::Jacobian  J_; 
    /// Jacobian in last moment, used to compute derivate of jacobian
    KDL::Jacobian  J_last; 

    ///The time at which the goal we saw on the last iteration was started
    //Referenced only in update loop
    double last_goal_starting_time_;
    
    typedef std::vector<ee_cart_imped_msgs::StiffPoint> EECartImpedTraj;
    
    ///Class for storing a trajectory and associated information
    //This is a class so as to be able to lock and unlock this data
    //together easily
    class EECartImpedData {
    public:
      ///Desired trajectory
      EECartImpedTraj traj;
      ///The point the robot was at before beginning trajectory
      ee_cart_imped_msgs::StiffPoint initial_point;
      ///The starting time of the trajectory
      ros::Time starting_time;
      EECartImpedData() {}
    };

    ///Desired positions
    //This needs to be in realtime boxes because
    //we reference it in the update loop and modify 
    //it it in the command callback
    realtime_tools::RealtimeBox<
      boost::shared_ptr<const EECartImpedData> > desired_poses_box_;
    
    ///The actual position of the robot when we achieved the last point on the trajectory
    //Referenced only in updated loop
    ee_cart_imped_msgs::StiffPoint last_point_;

    // Note the gains are incorrectly typed as a twist,
    // as there is no appropriate type!
    /// Proportional gains
    //Referenced only in update loop
    KDL::Twist     Kp_;
    /// Derivative gains
    //Referenced only in update loop
    KDL::Twist     Kd_;   
    
    /// Time of the last servo cycle
    //Modified in both command callback
    //and update loop, but it does not matter
    ros::Time last_time_;
    double dt;

    /// desired mass and damping term
    double M_des, D_des;

    /// Residual error for force estimation(vector in eigen form)
    JointVector r_e, sum_e;

    //Search range for nearest point search
    unsigned int s_range;

    /**
     * @brief KDL inertia matrix convert into eigen form
     * 
     * @param m inertia matrix in kdl form
     * @param mass inertia matrix in eigen form 
     */
    void InertiaMatrixKDLToEigen(KDL::JntSpaceInertiaMatrix &m, JointInertiaMatrix &mass);
    /**
     * @brief KDL joint vector convert into eigen form
     * 
     * @param a joint vector in kdl form
     * @param v joint vector in eigen form
     */
    void ArrayKDLToEigen(KDL::JntArray const &a, JointVector &v);
    /**
     * @brief KDL jacobian matrix convert into eigen form
     * 
     * @param jac jacobian matrix in kdl form
     * @param j_e jacobian matrix in eigen form
     */
    void JacobianMatrixKDLToEigen(KDL::Jacobian &jac, JacobianMatrix &j_e);
    /**
     * @brief KDL joint velocity vector convert into eigen form
     * 
     * @param a joint velocity vector in kdl form
     * @param v joint velocity vector in eigen form
     */
    void ArrayKDLToEigen(KDL::JntArrayVel const &a, JointVector &v);
    /**
     * @brief KDL twist convert into eigen form
     * @details [long description]
     * 
     * @param t kdl twist 
     * @param v twist in eigen form
     */
    void TwistKDLToEigen(KDL::Twist &t, Cart6Vector &v);

    /**
     * @brief simple function to do pesudo inverse
     * 
     * @param a matrix need to do pseudo inverse
     * @param epsilon a small number in inverse computation 
     * @param inv result of pseudo inverse
     */
    void PseudoInverse(CartesianInertiaMatrix const & a,
                       double epsilon,
                       CartesianInertiaMatrix & inv);

    /**
     * \brief Linearly interpolates between trajectory points
     *
     * Linearly interpolates between startValue and endValue over 
     * the time period startTime to endTime of the current goal.
     * @param time [seconds] that this goal has been running
     * @param startTime time that the goal began running
     * @param endTime time that the goal should finish running
     * @param startValue the values at the beginning of the interpolation (i.e. the values at startTime)
     * @param endValue the values at the end of the interpolation (i.e. the values at endTime)
     * @return ((time - startTime) * (endValue - startValue) / 
     (endTime - startTime)) + startValue; 
    */

    double linearlyInterpolate(double time, double startTime, 
			       double endTime, double startValue, 
			       double endValue);
    
    
    /**
     *\brief Callback when a new goal is received
     *
     *Cancels the currently active goal (if there is one), restarts
     *the controller and asks it to execute the new goal.
     *
     *@param msg the new goal
     *
     */
    void commandCB(const ee_cart_imped_msgs::EECartImpedGoalConstPtr &msg);
    ros::NodeHandle node_;
    ros::Subscriber subscriber_;
    

    /**
     *\brief Samples the interpolation and returns the point the
     * joints should currently be trying to achieve.
     *
     * Samples the interpolation, using 
     * EECartImpedControlClass::linearlyInterpolate, between the
     * current trajectory point and the next trajectory point.  Interpolation
     * is done for position and orientation, but not for wrench or stiffness
     * as generally it is desired that wrench or stiffness be constant
     * over a specific trajectory segment.
     *
     * @return A <A HREF=http://www.ros.org/doc/api/ee_cart_imped_msgs/html/msg/StiffPoint.html>StiffPoint</A> that is the point
     * the joints should attempt to achieve on this timestep.
     *
     */
    ee_cart_imped_msgs::StiffPoint sampleInterpolation();
    
    ///State publisher, published every 10 updates
    boost::scoped_ptr<
      realtime_tools::RealtimePublisher<
	ee_cart_imped_msgs::EECartImpedFeedback> > 
    controller_state_publisher_;
    
    ///The number of updates to the joint position
    //Referenced only in updates
    int updates_;

    /// mutex used to lock the data
    boost::mutex data_mutex_;
    /// use another thread to compute task torque
    boost::thread * genThread_;
    /// sign to control if the task torque thread open or not 
    bool working_;

    /**
     * @brief function to compute task torque, which contains the coriolis / gravity / mass compensation and external force compensation 
     * 
     * @param q joint postion vector
     * @param qdot joint velocity vector
     * @param timestep time gap between each loop 
     * @return task torque used in feedback linearization and force estimation 
     */
    JointVector computeTaskTorque( KDL::JntArray const &q, KDL::JntArrayVel const &qdot, double timestep);

    /// root link name and tip link name of the robot chain used in the controller
    std::string root_name, tip_name;
    /// kdtree used to find neareat point in reference trajectory
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
    /// initial position or current position 
    pcl::PointXYZ searchPoint;
    /// vector which store the trajectory position
    std::vector<KDL::Frame> demo_traj;
    /// vector which store the trajectory velocity
    std::vector<KDL::Twist> demo_traj_vel;

    /// Norm of velocity
    KDL::Twist flow_velocity;
    /// Norm of velocity in last time moment
    KDL::Twist flow_velocity_history;
    /// History phase information, represented by index of point in last time moment
    unsigned int point_history;
    
    /// Position of tip when we start the controller
    KDL::Frame init_pos;
  public:
    /**
     * \brief Controller initialization in non-realtime
     *
     * Initializes the controller on first startup.  Prepares the 
     * kinematics, pre-allocates the variables, subscribes to
     * command and advertises state.
     *
     * @param robot The current robot state
     * @param n A node handle for subscribing and advertising topics
     *
     * @return True on successful initialization, false otherwise
     *
     */
    bool init(pr2_mechanism_model::RobotState *robot,
	      ros::NodeHandle &n);
    /**
     * \brief Controller startup in realtime
     * 
     * Resets the controller to prepare for a new goal.  Sets the desired
     * position and orientation to be the current position and orientation
     * and sets the joints to have maximum stiffness so that they hold
     * their current position.
     */
    void starting();
    
    /**
     * \brief Controller update loop in realtime
     *
     * A PD controller for achieving the trajectory.
     * Uses EECartImpedControlClass::sampleInterpolation to
     * find the point that should be achieved on the current timestep.
     * Converts this point (which is in Cartesian coordinates) to joint
     * angles and uses a PD update (in the case of stiffness) to send
     * to the joints the correct force.
     *
     */
    void update();

    /**
     * \brief Controller stopping in realtime
     *
     * Calls EECartImpedControlClass::starting() to lock the joints into
     * their current position.
     *
     */
    void stopping();

    /**
     * @brief function bonded with task torque computation thread
     * 
     * @param sampling_freq sample frequency used in the computation loop of task torque
     */
    void doCalculation(double sampling_freq);
    
  };
}

#endif //ee_cart_imped_control.hpp
