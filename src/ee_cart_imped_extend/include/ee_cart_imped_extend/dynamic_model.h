#ifndef DYN_MOD_H
#define DYN_MOD_H

#include <kdl/jntarray.hpp>
#include <kdl/jntspaceinertiamatrix.hpp>
#include <kdl/chain.hpp>
#include <kdl/chaindynparam.hpp>
#include <boost/shared_ptr.hpp>

class DynamicsModel{
  public:
    //constructors 
    DynamicsModel();
    DynamicsModel(boost::shared_ptr<KDL::Chain> kdl_chain_ptr, boost::shared_ptr<KDL::Vector> grav_ptr);
    ~DynamicsModel();
    
    //joint torques
    void computeJointTorques( const std::vector<double> &q, 
                              const std::vector<double> &qdot, 
                              const std::vector<double> &qdotdot, 
                              std::vector<double> &torques);

    //dynamic parameters of a manipulator
    void getMassMatrix(const std::vector<double> &q_, Eigen::MatrixXd &Mass);
    void getCoriolisMatrix(const std::vector<double> &q_, const std::vector<double> &qdot_, Eigen::MatrixXd &Coriolis);
    void getGravityMatrix(const std::vector<double> &q_, Eigen::MatrixXd &Gravity);

  private:
    //ptrs
    boost::shared_ptr<KDL::Chain> kdl_chain_ptr_;
    boost::shared_ptr<KDL::Vector> grav_ptr_;
    boost::shared_ptr<KDL::ChainDynParam> cdp_ptr;
    boost::shared_ptr<KDL::ChainIdSolver_RNE> idsolver_ptr;
};
#endif
