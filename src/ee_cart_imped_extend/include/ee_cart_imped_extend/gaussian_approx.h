#ifndef GAUSSIAN_APPROX_H_
#define GAUSSIAN_APPROX_H_

#include "ee_cart_imped_extend/function_approx.h"
#include <iostream>
#include <Eigen/Core>
#include <Eigen/SVD>
#include <Eigen/LU>
#include <cmath>

namespace ee_cart_imped_extend {

const double PI = 3.14159265359;

/// Class for linear function approximation with the univariate Gaussian basis
class GaussianApprox : public FunctionApprox {
public:
	GaussianApprox(int num_bases);
	GaussianApprox(const std::vector<double> &w);
	virtual ~GaussianApprox();

	/**\brief Evaluate the function approximator at point x
	 * \param x The point at which to evaluate
	 * \return The scalar value of the function at x
	 */
	virtual double evalAt(double x);

	/**\brief Computes the least squares weights given a set of data points
	 * \param X A vector of the domain values of the points
	 * \param Y A vector of the target values of the points
	 */
	virtual void leastSquaresWeights(const std::vector<double> &X, const std::vector<double> &Y, int n_pts);

private:
	/**\brief Calculate the Gaussian basis features at point x
	 * \param x The point at which to get features
	 */
	void calcFeatures(double x);

	/**\brief Calculate the Moore-Penrose pseudoinverse of a matrix using SVD
	 * \param mat The matrix to pseudoinvert
	 * \return The pseudoinverted matrix
	 */
	Eigen::MatrixXd pseudoinverse(Eigen::MatrixXd mat);

	std::vector<double> features_;  //Storage for a set of features
	std::vector<double> centers_;   //Centers of RBFs
	std::vector<double> widths_;    //Widths of RBFs

};
typedef boost::shared_ptr<GaussianApprox> GaussianApproxPtr;
typedef boost::shared_ptr<const GaussianApprox> GaussianApproxConstPtr;

}

#endif
