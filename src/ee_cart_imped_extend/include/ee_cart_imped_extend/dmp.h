#ifndef DMP_H_
#define DMP_H_

#include "ros/ros.h"
#include "ee_cart_imped_extend/gaussian_approx.h"
#include "ee_cart_imped_extend/DMPTraj.h"
#include "ee_cart_imped_extend/DMPData.h"
#include "ee_cart_imped_extend/DMPPoint.h"
#include <cmath>
#include <fstream>
#include <algorithm>

namespace ee_cart_imped_extend {

/**
 * @brief calculate phase given current time and period for rhythmic movement
 * 
 * @param curr_time current time
 * @param tau period of movement
 * 
 * @return phase correspond to time
 */
double calcPhase(const double curr_time, const double tau);

/**
 * @brief Given a single demo trajectory, produces a multi-dim DMP
 * @param[in] demo An n-dim demonstration trajectory
 * @param[in] k_gains A proportional gain for each demo dimension
 * @param[in] d_gains A vector of differential gains for each demo dimension
 * @param[in] num_bases The number of basis functions to use for the fxn approx (i.e. the order of the Fourier series)
 * @param[out] dmp_list An n-dim list of DMPs that are all linked by a single canonical (phase) system
 */
void learnFromDemo(const DMPTraj &demo,
                   const std::vector<double> &k_gains,
                   const std::vector<double> &d_gains,
                   const int &num_bases,
                   std::vector<DMPData> &dmp_list);

/**
 * @brief Use the current active multi-dim DMP to create a plan starting from x_0 toward a goal
 * @param[in] dmp_list An n-dim list of DMPs that are all linked by a single canonical (phase) system
 * @param[in] x_0 The (n-dim) starting state for planning
 * @param[in] x_dot_0 The (n-dim) starting instantaneous change in state for planning
 * @param[in] goal The (n-dim) goal point for planning
 * @param[in] tau The time scaling constant (in this implementation, it is the desired length of the TOTAL (not just this segment) DMP execution in seconds)
 * @param[in] dt The desired time resolution of the plan
 * @param[out] plan An n-dim plan starting from x_0
 */
void generatePlan(const std::vector<DMPData> &dmp_list,
                  const std::vector<double> &x_0,
                  const std::vector<double> &x_dot_0,
                  const std::vector<double> &goal,
                  const double &tau,
                  const double &dt,
                  DMPTraj &plan);

}
#endif /* DMP_H_ */
