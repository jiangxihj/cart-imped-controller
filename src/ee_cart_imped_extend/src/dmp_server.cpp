#include "ee_cart_imped_extend/dmp.h"

// ROSBag
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/package.h>

// Boost
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

using namespace ee_cart_imped_extend;

std::vector<DMPData> active_dmp_list;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "dmp_server");
	ros::NodeHandle n;

	ROS_INFO("DMP services now ready");

	//Global parameter
	const int tau = 2;//movement period 1s
	const int dims = 2;
	const int n_bases = 20;
	const double dt = 0.001;//time step
	int n_pts = tau / dt + 1;
	double omega = (2 * PI) / tau;

	//Initialize the trajectory with ellipse
	DMPTraj demo_ellipse;
	DMPPoint demo_x, demo_y;
	for (int i = 0; i < n_pts; i++)
	{
		double x, y;
		x = -0.3 + 0.1 * sin(dt * i * omega);
		y = -0.2 + 0.1 * cos(dt * i * omega);
		// x = -0.30 + 0.1 * cos(dt * i * omega);
		// y = -0.15 + 0.1 * sin(dt * i * omega) * cos(dt * i * omega);
		DMPPoint temp_point;
		temp_point.positions.push_back(x);
		temp_point.positions.push_back(y);
		demo_ellipse.points.push_back(temp_point);
		demo_ellipse.times.push_back(dt * i);
	}

	//K and D term for spring damping system
	double K = 100;
	double D = 2 * sqrt(100);
	std::vector<double> k_vector;
	std::vector<double> d_vector;
	for (int i = 0; i < dims; i++)
	{
		k_vector.push_back(K);
		d_vector.push_back(D);
	}

	//Do dmp learning
	std::vector<DMPData> dmp_list;
	learnFromDemo(demo_ellipse, k_vector, d_vector, n_bases, dmp_list);
	for (int i = 0; i < dmp_list.size(); i++)
	{
		ROS_INFO_STREAM("Period of: " << i << "dmp is " << dmp_list[i].period);
		ROS_INFO_STREAM(dmp_list[i].weights[0]);

	}

	//Do dmp planning
	std::vector<double> x_0, x_dot_0, goal;
	for (int i = 0; i < dims; i++)
	{
		x_dot_0.push_back(0);
	}
	x_0.push_back(-0.3);
	x_0.push_back(-0.1);
	goal.push_back(-0.3);
	goal.push_back(-0.2);
	//Just reproduction
	double tau_g = tau;
	DMPTraj plan;
	generatePlan(dmp_list, x_0, x_dot_0, goal,
	             tau_g, dt,
	             plan);



	//Save demonstration and reproduction data into txt------------------------------------//
	std::ofstream outfile1("src/ee_cart_imped_extend/dmp_encode_trajectory/dmp_demo.txt");
	if (!outfile1)
	{
		std::cout << "Unable to open outfile";
		exit(1); // terminate with error
	}
	for (unsigned int i = 0; i < demo_ellipse.points.size(); i++)
	{
		outfile1 << i << "|" << demo_ellipse.points[i].positions[0] << "|" << demo_ellipse.points[i].positions[1] << "|" << demo_ellipse.times[i] << std::endl;
	}
	outfile1.close();

	std::ofstream outfile2("src/ee_cart_imped_extend/dmp_encode_trajectory/dmp_repo_circle.txt");
	if (!outfile2)
	{
		std::cout << "Unable to open outfile";
		exit(1); // terminate with error
	}
	for (unsigned int i = 0; i < plan.points.size(); i++)
	{
		outfile2 << i << "|" << plan.points[i].positions[0] << "|" << plan.points[i].positions[1]
		         << "|" << plan.points[i].velocities[0] << "|" << plan.points[i].velocities[1]
		         << "|" << plan.times[i] << std::endl;
	}
	outfile2.close();

	return 0;
}
