#include "ee_cart_imped_extend/dynamic_model.h"

# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <cmath>
# include <ctime>

#include <Eigen/Core>
#include <Eigen/LU>

#include <boost/make_shared.hpp>
#include <boost/array.hpp>

DynamicsModel::DynamicsModel(boost::shared_ptr<KDL::Chain> kdl_chain_ptr, boost::shared_ptr<KDL::Vector> grav_ptr) 
                              : kdl_chain_ptr_(kdl_chain_ptr), 
                                grav_ptr_(grav_ptr),
                                cdp_ptr(new KDL::ChainDynParam(*kdl_chain_ptr_, *grav_ptr_)),
                                idsolver_ptr(new KDL::ChainIdSolver_RNE(*kdl_chain_ptr_, *grav_ptr_))
{
}

DynamicsModel::~DynamicsModel() {}

void DynamicsModel::getMassMatrix(const std::vector<double> &q_, Eigen::MatrixXd &Mass)
{
  KDL::JntArray q(kdl_chain_ptr_->getNrOfJoints());
  for(size_t i=0; i<kdl_chain_ptr_->getNrOfJoints(); i++)
    q.data(i,0) = q_.at(i);
  KDL::JntSpaceInertiaMatrix M(kdl_chain_ptr_->getNrOfJoints());
  cdp_ptr->JntToMass(q, M);
  Mass=Eigen::MatrixXd::Identity(M.data.rows(),M.data.cols());
  for(int i=0; i<M.data.rows(); i++)
  {
    for(int j=0; j<M.data.cols(); j++)
    {
      Mass(i,j)=M.data(i,j);
    }
  }
}

void DynamicsModel::getCoriolisMatrix(const std::vector<double> &q_, const std::vector<double> &qdot_, Eigen::MatrixXd &Coriolis)
{
  KDL::JntArray q(kdl_chain_ptr_->getNrOfJoints());
  KDL::JntArray qdot(kdl_chain_ptr_->getNrOfJoints());
  for(size_t i=0; i<kdl_chain_ptr_->getNrOfJoints(); i++){
    q.data(i,0) = q_.at(i);
    qdot.data(i,0) = qdot_.at(i);
  }
  KDL::JntArray C(kdl_chain_ptr_->getNrOfJoints());
  cdp_ptr->JntToCoriolis(q, qdot, C);
  Coriolis=Eigen::MatrixXd::Identity(C.data.rows(),C.data.cols());
  for(int i=0; i<C.data.rows(); i++)
  {
    for(int j=0; j<C.data.cols(); j++)
    {
      Coriolis(i,j)=C.data(i,j);
    }
  }
}

void DynamicsModel::getGravityMatrix(const std::vector<double> &q_, Eigen::MatrixXd &Gravity)
{
  KDL::JntArray q(kdl_chain_ptr_->getNrOfJoints());
  for(size_t i=0; i<kdl_chain_ptr_->getNrOfJoints(); i++)
    q.data(i,0) = q_.at(i);
  KDL::JntArray G(kdl_chain_ptr_->getNrOfJoints());
  cdp_ptr->JntToGravity(q, G);
  Gravity=Eigen::MatrixXd::Identity(G.data.rows(),G.data.cols());
  for(int i=0; i<G.data.rows(); i++)
  {
    for(int j=0; j<G.data.cols(); j++)
    {
      Gravity(i,j)=G.data(i,j);
    }
  }
}

void DynamicsModel::computeJointTorques(const std::vector<double> &q, 
                                          const std::vector<double> &qdot, 
                                          const std::vector<double> &qdotdot_in, 
                                          std::vector<double> &torques)
{
  //TODO assert the dimensions are correct 

  KDL::JntArray kdl_q(kdl_chain_ptr_->getNrOfJoints());
  KDL::JntArray kdl_qdot(kdl_chain_ptr_->getNrOfJoints());
  KDL::JntArray kdl_qdotdot(kdl_chain_ptr_->getNrOfJoints());
  
  //dynamic parameters for computing joint torque
  KDL::JntSpaceInertiaMatrix kdl_M(kdl_chain_ptr_->getNrOfJoints());
  KDL::JntArray kdl_C(kdl_chain_ptr_->getNrOfJoints());
  KDL::JntArray kdl_G(kdl_chain_ptr_->getNrOfJoints());
  KDL::JntArray kdl_tauMCG(kdl_chain_ptr_->getNrOfJoints());

  KDL::SetToZero(kdl_q);
  KDL::SetToZero(kdl_qdot);
  KDL::SetToZero(kdl_qdotdot);

  for(size_t i=0; i<kdl_q.rows(); i++)
  {
    kdl_q(i)=q[i];
    kdl_qdot(i)=qdot[i];
    kdl_qdotdot(i)=qdotdot_in[i];
  }

  cdp_ptr->JntToMass(kdl_q, kdl_M);
  cdp_ptr->JntToCoriolis(kdl_q, kdl_qdot, kdl_C);
  cdp_ptr->JntToGravity(kdl_q, kdl_G);
  
  //M * qdd + C + G = Tau
  kdl_tauMCG.data = kdl_M.data.lazyProduct(kdl_qdotdot.data) + kdl_C.data + kdl_G.data;
  
  //set return container
  torques.resize(kdl_tauMCG.rows());
  for(size_t i=0; i<kdl_tauMCG.rows(); i++)
    torques[i] = kdl_tauMCG(i);
}
