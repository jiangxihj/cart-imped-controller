#include "ee_cart_imped_extend/gaussian_approx.h"
#include <stdio.h>
using namespace Eigen;
using namespace std;

namespace ee_cart_imped_extend {

GaussianApprox::GaussianApprox(int num_bases)
{
	n_bases = num_bases;
	features_.resize(n_bases);
	centers_.resize(n_bases);
	widths_.resize(n_bases);
	weights_.resize(n_bases);
	for (int i = 0; i < n_bases; i++) {
		features_[i] = 0;
		centers_[i] = static_cast<double> (i) / static_cast<double> (n_bases);
		centers_[i] *= 2 * PI;
		widths_[i] = 0.1 * n_bases * n_bases;
	}
}

GaussianApprox::GaussianApprox(const vector<double> &w)
{
	weights_ = w;
	n_bases = w.size();
	features_.resize(n_bases);
	centers_.resize(n_bases);
	widths_.resize(n_bases);
	for (int i = 0; i < n_bases; i++) {
		features_[i] = 0;
		centers_[i] = static_cast<double> (i) / static_cast<double> (n_bases);
		centers_[i] *= 2 * PI;
		widths_[i] = 0.1 * n_bases * n_bases;
	}
}


GaussianApprox::~GaussianApprox()
{
}


double GaussianApprox::evalAt(double x)
{
	calcFeatures(x);

	double wsum = 0;
	for (int i = 0; i < n_bases; i++) {
		wsum += features_[i] * weights_[i];
	}
	return wsum;
}


void GaussianApprox::leastSquaresWeights(const std::vector<double> &X, const std::vector<double> &Y, int n_pts)
{
	MatrixXd D_mat = MatrixXd(n_pts, n_bases);
	MatrixXd Y_mat = MatrixXd(n_pts, 1);

	//Calculate the design matrix
	for (int i = 0; i < n_pts; i++)
	{
		Y_mat(i, 0) = Y[i];
		calcFeatures(X[i]);
		for (int j = 0; j < n_bases; j++)
		{
			D_mat(i, j) = features_[j];
		}
	}

	//Calculate the least squares weights via projection onto the basis functions
	MatrixXd w = pseudoinverse(D_mat.transpose() * D_mat) * D_mat.transpose() * Y_mat;
	for (int i = 0; i < n_bases; i++) {
		weights_[i] = w(i, 0);
	}
}


void GaussianApprox::calcFeatures(double x)
{
	double sum = 0;
	for (int i = 0; i < n_bases; i++) {
		//features[i] = exp(-widths[i] * (x - centers[i]) * (x - centers[i]));
		features_[i] = exp(widths_[i] * (cos(x - centers_[i]) - 1));
		sum += features_[i];
	}
	for (int i = 0; i < n_bases; i++) {
		features_[i] /= sum;
	}
}


MatrixXd GaussianApprox::pseudoinverse(MatrixXd mat) {
	//Numpy uses 1e-15 by default.  I use 1e-10 just to be safe.
	double precisionCutoff = 1e-10;

	//Compute the SVD of the matrix
	JacobiSVD<MatrixXd> svd(mat, ComputeThinU | ComputeThinV);
	MatrixXd U = svd.matrixU();
	MatrixXd V = svd.matrixV();
	MatrixXd S = svd.singularValues();

	//Psuedoinvert the diagonal matrix of singular values
	MatrixXd S_plus = MatrixXd::Zero(n_bases, n_bases);
	for (int i = 0; i < n_bases; i++) {
		if (S(i) > precisionCutoff) { //Cutoff to avoid huge inverted values for numerical stability
			S_plus(i, i) = 1.0 / S(i);
		}
	}

	//Compute psuedoinverse of orginal matrix
	return V * S_plus * U.transpose();
}

}


