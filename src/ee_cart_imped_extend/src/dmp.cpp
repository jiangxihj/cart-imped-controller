#include "ee_cart_imped_extend/dmp.h"
using namespace std;

namespace ee_cart_imped_extend {

#define MAX_PLAN_LENGTH 1000

double alpha = -log(0.01); //Ensures 99% phase convergence at t=tau

/**
 * @brief calculate phase given current time and period for rhythmic movement
 * 
 * @param curr_time current time
 * @param tau period of movement
 * 
 * @return phase correspond to time
 */
double calcPhase(double curr_time, double tau)
{
	//return exp(-(alpha/tau)*curr_time);
	return curr_time / tau;
}


/**
 * @brief Given a single demo trajectory, produces a multi-dim DMP
 * @param[in] demo An n-dim demonstration trajectory
 * @param[in] k_gains A proportional gain for each demo dimension
 * @param[in] d_gains A vector of differential gains for each demo dimension
 * @param[in] num_bases The number of basis functions to use for the fxn approx (i.e. the order of the Fourier series)
 * @param[out] dmp_list An n-dim list of DMPs that are all linked by a single canonical (phase) system
 */
void learnFromDemo(const DMPTraj &demo,
                   const vector<double> &k_gains,
                   const vector<double> &d_gains,
                   const int &num_bases,
                   vector<DMPData> &dmp_list)
{
	//Determine traj length and dim
	int n_pts = demo.points.size();
	if (n_pts < 1) {
		ROS_ERROR("Empty trajectory passed to learn_dmp_from_demo service!");
		return;
	}
	int dims = demo.points[0].positions.size();
	double tau = demo.times[n_pts - 1];

	std::vector<double> f_domain, f_targets, x_demo, v_demo, v_dot_demo;
	x_demo.resize(n_pts);
	v_demo.resize(n_pts);
	v_dot_demo.resize(n_pts);
	f_domain.resize(n_pts);
	f_targets.resize(n_pts);
	FunctionApprox *f_approx = new GaussianApprox(num_bases);

	//Compute the DMP weights for each DOF separately
	for (int d = 0; d < dims; d++)
	{
		double curr_k = k_gains[d];
		double curr_d = d_gains[d];

		//Compute the goal for one dimension, use average of max+min
		std::vector<double> position_series;
		for (int i = 0; i < demo.points.size(); i++)
		{
			position_series.push_back(demo.points[i].positions[d]);
		}
		std::vector<double>::iterator biggest = std::max_element(position_series.begin(), position_series.end());
		std::vector<double>::iterator smallest = std::min_element(position_series.begin(), position_series.end());
		double goal = (*biggest + *smallest) / 2;
		ROS_INFO_STREAM("The goal of learning process is: " << goal);

		x_demo[0] = demo.points[0].positions[d];
		v_demo[0] = 0;
		v_dot_demo[0] = 0;

		//Calculate the demonstration v and v dot by assuming constant acceleration over a time period
		for (int i = 1; i < n_pts; i++)
		{
			x_demo[i] = demo.points[i].positions[d];
			double dx = x_demo[i] - x_demo[i - 1];
			double dt = demo.times[i] - demo.times[i - 1];
			v_demo[i] = dx / dt;
			v_dot_demo[i] = (v_demo[i] - v_demo[i - 1]) / dt;
		}


		//Calculate the target pairs so we can solve for the weights
		for (int i = 0; i < n_pts; i++)
		{
			double phase = calcPhase(demo.times[i], tau);
			f_domain[i] = phase * 2 * PI; //Scaled time is cleaner than phase for spacing reasons
			f_targets[i] = (tau * tau * v_dot_demo[i] + curr_d * tau * v_demo[i]) - curr_k * (goal - x_demo[i]);
		}

		//Solve for weights
		f_approx->leastSquaresWeights(f_domain, f_targets, n_pts);

		//Create the DMP structures
		DMPData *curr_dmp = new DMPData();
		curr_dmp->weights = f_approx->getWeights();
		curr_dmp->k_gain = curr_k;
		curr_dmp->d_gain = curr_d;
		curr_dmp->n_pts = n_pts;
		curr_dmp->period = tau;
		for (int i = 0; i < n_pts; i++)
		{
			curr_dmp->f_domain.push_back(f_domain[i]);
			curr_dmp->f_targets.push_back(f_targets[i]);
		}
		dmp_list.push_back(*curr_dmp);
	}
	delete f_approx;
}



/**
 * @brief Use the current active multi-dim DMP to create a plan starting from x_0 toward a goal
 * @param[in] dmp_list An n-dim list of DMPs that are all linked by a single canonical (phase) system
 * @param[in] x_0 The (n-dim) starting state for planning
 * @param[in] x_dot_0 The (n-dim) starting instantaneous change in state for planning
 * @param[in] goal The (n-dim) goal point for planning
 * @param[in] tau The time scaling constant (in this implementation, it is the desired length of the TOTAL (not just this segment) DMP execution in seconds)
 * @param[in] dt The desired time resolution of the plan
 * @param[out] plan An n-dim plan starting from x_0
 */
void generatePlan(const vector<DMPData> &dmp_list,
                  const vector<double> &x_0,
                  const vector<double> &x_dot_0,
                  const vector<double> &goal,
                  const double &tau,
                  const double &dt,
                  DMPTraj &plan)
{
	plan.points.clear();
	plan.times.clear();

	int dims = dmp_list.size();
	int n_pts = 0;

	std::vector< std::vector<double> > x_vecs(dims);
	std::vector< std::vector<double> > x_dot_vecs(dims);
	std::vector<double> t_vec;
	std::vector<FunctionApproxPtr> f(dims);
	for (int i = 0; i < dims; i++)
	{
		f[i] = GaussianApproxPtr(new GaussianApprox(dmp_list[i].weights));
	}

	double t = 0;
	double f_eval;
	while (t < tau)
	{
		for (int i = 0; i < dims; i++)
		{
			double x, v;
			if (n_pts == 0)
			{
				x = x_0[i];
				v = x_dot_0[i];
			}
			else
			{
				x = x_vecs[i][n_pts - 1];
				v = x_dot_vecs[i][n_pts - 1] * dmp_list[i].period;
			}
			double s = calcPhase(t, dmp_list[i].period);
			ROS_INFO_STREAM("Phase term: " << s);
			f_eval = f[i]->evalAt(s * 2 * PI);
			double v_dot = (dmp_list[i].k_gain * (goal[i] - x) - dmp_list[i].d_gain * v + f_eval) / dmp_list[i].period;
			double x_dot = v / dmp_list[i].period;
			//Update state variables
			v += v_dot * dt;
			x += x_dot * dt;
			//Add current state to the plan
			x_vecs[i].push_back(x);
			x_dot_vecs[i].push_back(v / dmp_list[i].period);
		}

		t += dt;
		t_vec.push_back(t);
		n_pts++;
	}

	//Create a plan from the generated trajectories
	plan.points.resize(n_pts);
	for (int j = 0; j < n_pts; j++)
	{
		plan.points[j].positions.resize(dims);
		plan.points[j].velocities.resize(dims);
	}
	for (int i = 0; i < dims; i++)
	{
		for (int j = 0; j < n_pts; j++)
		{
			plan.points[j].positions[i] = x_vecs[i][j];
			plan.points[j].velocities[i] = x_dot_vecs[i][j];
		}
	}
	plan.times = t_vec;

}

}

