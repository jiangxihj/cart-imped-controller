#include "ee_cart_imped_extend/dynamic_model.h"
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/tree.hpp>
#include <kdl/chain.hpp>
#include <boost/make_shared.hpp>
#include <boost/scoped_ptr.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/jacobian.hpp>

void print_stl_vec( std::string name, std::vector<double> vec)
{
  std::cout << name << ": [ ";
  for (size_t i = 0; i < vec.size(); i++)
    std::cout << vec.at(i) << " ";
  std::cout << "]" << std::endl;
}

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "test_dynamic_model");
  ros::NodeHandle nh("~");

  //get robot state
  std::cout << "Getting the urdf..." << std::endl;
  std::string robot_description;
  std::string robot_param;

  nh.searchParam("robot_description", robot_param);
  nh.param<std::string>(robot_param, robot_description, "");

  //check the param server return
  if (robot_description.empty())
  {
    std::cout << "Failed to retreive robot urdf" << std::endl;
    return 0;
  }

  //parse the KDL tree
  KDL::Tree kdl_tree;
  if (!kdl_parser::treeFromString(robot_description, kdl_tree))
  {
    std::cout << "Failed to parse the kdl tree" << std::endl;
    return 0;
  }

  //parse the KDL chain
  KDL::Chain planning_chain;
  if (!kdl_tree.getChain("torso_lift_link", "r_wrist_roll_link", planning_chain))
  {
    std::cout << "Failed to parse the kdl chain" << std::endl;
    return 0;
  }
  boost::shared_ptr<KDL::Chain> kdl_chain_ptr = boost::make_shared<KDL::Chain>(planning_chain);
  std::cout << "KDL chain has " << kdl_chain_ptr->getNrOfSegments() << " segments and " << kdl_chain_ptr->getNrOfJoints() << " joints." << std::endl;

  std::cout << "Joints: ";
  for (unsigned int i = 0; i < kdl_chain_ptr->getNrOfSegments(); i++)
    std::cout << kdl_chain_ptr->segments.at(i).getJoint().getName() << " ";
  std::cout << std::endl;

  boost::shared_ptr<KDL::Vector> grav_ptr = boost::make_shared<KDL::Vector>(0.0, 0.0, -9.81);
  //boost::shared_ptr<KDL::Vector> grav_ptr = boost::make_shared<KDL::Vector>(0.0, 0.0, 0.0);
  std::cout << "\tCreating dynamics model." << std::endl;
  boost::shared_ptr<DynamicsModel> dyn_model_ptr = boost::make_shared<DynamicsModel>(kdl_chain_ptr, grav_ptr);

  std::cout << "Initializing robot arm states for dynamics..." << std::endl;
  std::vector<double> q_, qdot_, qdotdot_;
  q_.resize(kdl_chain_ptr->getNrOfJoints());
  qdot_.resize(kdl_chain_ptr->getNrOfJoints());
  qdotdot_.resize(kdl_chain_ptr->getNrOfJoints());

  //std::fill(q_.begin(), q_.end(), 1);
  q_.clear();
  q_.push_back(0.501591);
  q_.push_back(0.580674);
  q_.push_back(-2.357578);
  q_.push_back(-1.837947);
  q_.push_back(0.953085);
  q_.push_back(-0.103487);
  q_.push_back(-3.018020);

  std::fill(qdot_.begin(), qdot_.end(), 0);
  std::fill(qdotdot_.begin(), qdotdot_.end(), 0);
  //Select input joint torques
  std::cout << "Setting input joint torques..." << std::endl;

  Eigen::MatrixXd Mass, Coriolis, Gravity;
  dyn_model_ptr->getMassMatrix(q_, Mass);
  dyn_model_ptr->getCoriolisMatrix(q_, qdot_, Coriolis);
  dyn_model_ptr->getGravityMatrix(q_, Gravity);
  std::vector<double> torques;
  dyn_model_ptr->computeJointTorques(q_, qdot_, qdotdot_, torques);

  for (unsigned int i = 0; i < torques.size(); i++)
  {
    ROS_INFO_STREAM("torques " << i << " is: " << torques[i]);
  }

  boost::scoped_ptr<KDL::ChainJntToJacSolver> jnt_to_jac_solver_;
  jnt_to_jac_solver_.reset(new KDL::ChainJntToJacSolver(planning_chain));
  KDL::Jacobian  J_;
  J_.resize(planning_chain.getNrOfJoints());
  KDL::JntArray q_j(kdl_chain_ptr->getNrOfJoints());
  for (size_t i = 0; i < kdl_chain_ptr->getNrOfJoints(); i++)
    q_j.data(i, 0) = q_.at(i);
  jnt_to_jac_solver_->JntToJac(q_j, J_);

  for (unsigned int i = 0 ; i < kdl_chain_ptr->getNrOfJoints() ; i++)
  {
    for (unsigned int j = 0 ; j < 6 ; j++)
    {
      ROS_INFO_STREAM(J_(j, i));
    }

  }


  return 0;
}
